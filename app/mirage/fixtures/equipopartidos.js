//import Mirage/*, {faker} */ from 'ember-cli-mirage';

export default [
	{"id":1,"local_id":1,"visitante_id":2, "partido_id":1,"ganador_id":1},
	{"id":2,"local_id":3,"visitante_id":4, "partido_id":2,"ganador_id":4},
	{"id":3,"local_id":5,"visitante_id":6, "partido_id":3,"ganador_id":6},
	{"id":4,"local_id":7,"visitante_id":8, "partido_id":4,"ganador_id":7},
	{"id":5,"local_id":9,"visitante_id":10, "partido_id":5,"ganador_id":10},
	{"id":6,"local_id":11,"visitante_id":12, "partido_id":6,"ganador_id":11},
	{"id":7,"local_id":13,"visitante_id":14, "partido_id":7,"ganador_id":14},
	{"id":8,"local_id":15,"visitante_id":16, "partido_id":8,"ganador_id":16},
	{"id":9,"local_id":1,"visitante_id":4, "partido_id":9,"ganador_id":1},
	{"id":10,"local_id":6,"visitante_id":7, "partido_id":10,"ganador_id":6},
	{"id":11,"local_id":10,"visitante_id":11, "partido_id":11,"ganador_id":11},
	{"id":12,"local_id":14,"visitante_id":16, "partido_id":12,"ganador_id":14},
	{"id":13,"local_id":1,"visitante_id":6, "partido_id":13,"ganador_id":6},
	{"id":14,"local_id":11,"visitante_id":14, "partido_id":14,"ganador_id":11},
	{"id":15,"local_id":6,"visitante_id":11, "partido_id":15,"ganador_id":11}];
