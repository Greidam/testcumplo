/*
  This is an example factory definition.

  Create more files in this directory to define additional factories.
*/
//import Mirage/*, {faker} */ from 'ember-cli-mirage';

export default [
  {"id":1,"nombre":"Aquiles Brinco","equipo_id":1}, 
  {"id":2,"nombre":"Armando Casas","equipo_id":2}, 
  {"id":3,"nombre":"Elba Zurita","equipo_id":3}, 
  {"id":4,"nombre":"Luz Roja","equipo_id":4}, 
  {"id":5,"nombre":"Marcia Ana","equipo_id":5}, 
  {"id":6,"nombre":"Susana Orio","equipo_id":6},
  {"id":7,"nombre":"Aquiles Castro","equipo_id":7}, 
  {"id":8,"nombre":"Zacaria Flores del Campo","equipo_id":8}, 
  {"id":9,"nombre":"Sole Dolio","equipo_id":9},
  {"id":10,"nombre":"Armando Mocha","equipo_id":10},
  {"id":11,"nombre":"Guillermo Nigote","equipo_id":11},
  {"id":12,"nombre":"Elvis Tek","equipo_id":12}, 
  {"id":13,"nombre":"Sevelinda Parada","equipo_id":13},
  {"id":14,"nombre":"Jose Luis Lamata Feliz","equipo_id":14}, 
  {"id":15,"nombre":"Zampa Teste","equipo_id":15},
  {"id":16,"nombre":"Miren Amiano","equipo_id":16}];
  // name: 'Pete',                         // strings
  // age: 20,                              // numbers
  // tall: true,                           // booleans

  // email: function(i) {                  // and functions
  //   return 'person' + i + '@test.com';
  // },

  // firstName: faker.name.firstName,       // using faker
  // lastName: faker.name.firstName,
  // zipCode: faker.address.zipCode
