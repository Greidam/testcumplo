export default function() {
//	this.get('posts');
//	this.get('posts/:id');
//collection of data
this.get('/posts', function(db, request) {
  return {
    data: db.posts.map(attrs => (
      {type: 'posts', id: attrs.id, attributes: attrs }
    ))
  };
});

//single data
this.get('/posts/:id', function(db, request) {
  let id = request.params.id;

  return {
    data: {
      type: 'posts',
      id: id,
      attributes: db.posts.find(id)
    }
  };
});


//collection of data
this.get('/equipos', function(db, request) {
  return {
    data: db.equipos.map(attrs => (
      {type: 'equipos', id: attrs.id, attributes: attrs }
    ))
  };
});

//single data
this.get('/equipos/:id', function(db, request) {
  let id = request.params.id;

  return {
    data: {
      type: 'equipos',
      id: id,
      attributes: db.equipos.find(id)
    }
  };
});


//collection of data
this.get('/partidos', function(db, request) {
  return {
    data: db.partidos.map(attrs => (
      {type: 'partidos', id: attrs.id, attributes: attrs }
    ))
  };
});

//single data
this.get('/partidos/:id', function(db, request) {
  let id = request.params.id;

  return {
    data: {
      type: 'partidos',
      id: id,
      attributes: db.partidos.find(id)
    }
  };
});





}
