import DS from 'ember-data';

export default DS.Model.extend({
  	nombre: DS.attr('string'),
	jugador: DS.belongsTo('jugadors'),
	equipopartido: DS.hasMany('equipopartidos', {async: true} )
});
