import DS from 'ember-data';

export default DS.Model.extend({
  	tipopartido: DS.belongsTo('tipopartidos'),
	equipopartidos: DS.hasMany('equipopartidos', {async: true} )
});

