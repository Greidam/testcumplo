import DS from 'ember-data';


export default DS.Model.extend({
	 local: DS.belongsTo('equipos', { async: true }),
  visitante: DS.belongsTo('equipos', { async: true }),
  partido: DS.belongsTo('partidos', { async: true }),
  ganador: DS.belongsTo('equipos', { async: true })
  
});
