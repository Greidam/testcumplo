import DS from 'ember-data';

export default DS.Model.extend({
  	id: DS.attr('int'),
	nombre: DS.attr('string')
});
