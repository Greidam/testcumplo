import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  //this.route('posts', {path: '/posts/:post_id'});
  this.route('equipos');
  this.route('partidos');
  this.route('tipopartidos');
 this.route('equipopartidos');
  this.route('jugadors');

this.resource('partidos', { path: '/partido/:partido_id' },   function() {
    this.resource('tipopartidos', function() {
    });
     this.resource('equipopartidos', function() {
     	 this.resource('equipos', function() {
     	 	 	 this.resource('jugadors', function() {
     	 		});
    	});
    });
  });

});


export default Router;
